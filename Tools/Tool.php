<?php

use Namshi\JOSE\JWS as JWS;
use Namshi\JOSE\SimpleJWS;

class Tool {

    //encrypt or decrypt string or array values
    public function encrypt_decrypt(String $action, $data){

        switch ($action) {

            case 'encrypt':
                
                $data = $this->array_encode($data);
                $cipher_method = 'aes-128-ctr';
                $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
                $enc_iv  = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher_method));
                $crypted_token = openssl_encrypt($data, $cipher_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);

                return $crypted_token;

                break;

            case 'decrypt':
                
                list($data, $enc_iv) = explode("::", $data);;
                $cipher_method = 'aes-128-ctr';
                $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
                $token = openssl_decrypt($data, $cipher_method, $enc_key, 0, hex2bin($enc_iv));

                $token = $this->array_decode($token);

                return $token;
                
                break;

            default:

                return 'Action must be either encrypt or decrypt.';

                break;
        }
    }

    public function array_encode($data){

        return base64_encode(serialize($data));
    }

    public function array_decode($data){

        return unserialize(base64_decode($data));
    }
  
    public function match_encrypted($string, $encrypted) {

        return ($this->encrypt_decrypt('decrypt', $encrypted) == $string ? true : false);
    }
  
  
    public function generateAuthToken($apiKey, $requestToken){

        return $this->encrypt_decrypt("encrypt", $apiKey. $requestToken);
    }
  
    public function decryptAuthToken($requestToken , $alias = ['api_key', 'verification_key']){

        $token = $this->encrypt_decrypt("decrypt", $requestToken);
        $res[$alias[0]] = substr($token, 0, 32);
        $res[$alias[1]] = substr($token, 32, 64);

        return $res;
    }


    public function generateToken(){

        return md5(uniqid(rand(), true));
    }

    public function randomString($l = 8){

        return substr(md5(uniqid(mt_rand(), true)), 0, $l);
    }    
  
    public function generateJWS ($user, $tokenId, $param = null){

        $issuedAt   = time();
        $notBefore  = $issuedAt  + 10;    // Adding 10 seconds
        $expire     = $notBefore + 600;   // Adding 60 minutes
        $serverName = 'EC2 instance, apache2 linux';    // Retrieve the server name from config file
    
         /*
         * Create the token as an array
         */
        $data = [
          'iat'  => $issuedAt,   // Issued at: time when the token was generated
          'jti'  => $tokenId,    // Json Token Id: an unique identifier for the token
          'iss'  => $serverName, // Issuer
          'nbf'  => $notBefore,  // Not before
          'exp'  => $expire,     // Expire
          'data' => $user
        ];
    
        $jws = new SimpleJWS(array('alg' => 'RS256'));
        $jws->setPayload($data);

        if(!$jws){
            echo ["code" => "0001", "message" => "JWS Error : ".json_encode($data)];
        }
      
        $jws->sign(file_get_contents(__DIR__."/../key/private.key"), 'tests');

        $token = $jws->getTokenString();

        return $token;
    }

}