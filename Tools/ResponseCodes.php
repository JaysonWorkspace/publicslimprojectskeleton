<?php

Class ResponseCodes{

	protected static $Error = [

		'0000' => 'Unknown Error',
		'0001' => 'Unexpected Error',
		'0002' => 'Invalid Token',
		'0003' => 'Unauthorized Actions',
		'0004' => 'Invalid Username or Password',
		'0005' => 'Deactivated Account, Please Contact Your Administrator',
		'0006' => 'Unable to add',
		'0007' => 'Unable to update',
		'0008' => 'Nothing to update',
		'0009' => 'Unable to delete',
		'0010' => 'No Results Found',
		'0011' => 'Username already exists'
		/*
		Add custom message
		.
		.
		.
		*/
	];

	//Add custom message
	protected static $Success = [

		'1000' => 'Successfully Added',
		'1001' => 'Successfully Updated',
		'1002' => 'Successfully Deleted'
		/*
		Add custom message
		.
		.
		.
		*/
	];

	public static function get(int $type, string $code){

		$result = [];

		$getCodes = [];

		switch ($type) {
		    case 0:
		    	$getCodes = self::$Error;
		        break;
		    case 1:
		    	$getCodes = self::$Success;
		        break;
		    default:
		    	$getCodes = [];
		}

		if(empty($getCodes)){

			$result['code']    = '00000';
			$result['message'] = 'Message type id '.$type. ' is invalid';

			return $result;
		}

		foreach ($getCodes as $key => $value) {
			
			if($key == $code){

				$result['code'] = $key;
				$result['message'] = $value;
				return $result;
				
			}
		}

		$result['code']    = '00001';
		$result['message'] = 'Message code id '.$code. ' is invalid';

		return $result;
	}
	

}