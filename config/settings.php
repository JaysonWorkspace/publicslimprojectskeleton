<?php
date_default_timezone_set('UTC');

class AppSettings{

    public $Settings;

    public function __construct(){

        $this->Settings = $this->getSettings();
    }

    public function __invoke() { 

        return $this->Settings;
    } 

    public function getSettings(){

        return [
            'settings' => [
                // Slim Settings
                'determineRouteBeforeAppMiddleware' => false,
                'displayErrorDetails' => true,

                // View settings
                'view' => [
                    'template_path' => __DIR__ . '/../templates',
                    'twig' => [
                        'cache' => __DIR__ . '/../cache/twig',
                        'debug' => true,
                        'auto_reload' => true,
                    ],
                ],
                // monolog settings
                'logger' => [
                    'name' => 'app',
                    'path' => __DIR__ . '/../log/app.log',
                ],
                'database' => [
                    "database_type" => "mysql",
                    "database_name" => "slimproject",
                    "server"        => "localhost",
                    "username"      => "root",
                    "password"      => "",
                    "charset"       => "utf8"
                ],
                //databases...
                'database2' => [
                    "database_type" => "mysql",
                    "database_name" => "slimproject",
                    "server"        => "localhost",
                    "username"      => "root",
                    "password"      => "",
                    "charset"       => "utf8"
                ]

            ]
        ];

    }

}