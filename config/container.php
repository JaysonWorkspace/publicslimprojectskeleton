<?php 

	class Container{

		static protected $container;

		public static function get(string $id){

			if(array_key_exists($id, self::$container)){
				return self::$container[$id];
			}else{
				return 'Dependency Injection '.$id.' not exist';
			}

			
		}

		public static function set($container){

	        self::$container = $container;
	    }


	}