<?php

$app->group('', function() use ($app){

    $app->post('/login', 			'Module\Controllers\LoginController:login');

});

$app->group('', function() use ($app){

    $app->get('/', 					'Module\Controllers\TestController:hello');
    $app->get('/getUsers', 			'Module\Controllers\TestController:getUsers');
    $app->post('/addUser', 			'Module\Controllers\TestController:addUser');
    $app->post('/updateUser', 		'Module\Controllers\TestController:updateUser');
    $app->post('/deleteUser', 		'Module\Controllers\TestController:deleteUser');

});


/*
- Route with Middleware
- Header are require to access this route.

Example:
header key : AUTHORIZATION
header value : eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.eyJpYXQiOjE1OTM5NjMxNjUsImp0aSI6ImVmMDU0MzMzZTkyNjU1ZGU4NjgxYTA1NWNmZjQyZGI2IiwiaXNzIjoiRUMyIGluc3RhbmNlLCBhcGFjaGUyIGxpbnV4IiwibmJmIjoxNTkzOTYzMTc1LCJleHAiOjE1OTM5NjM3NzUsImRhdGEiOnsidXNlcl9pZCI6IjEiLCJ1c2VybmFtZSI6ImFkbWluMTEiLCJwYXNzd29yZCI6ImJzMCszWllxQjZsYm93R3FZMDZmNmJyQkVncz06OmRkMjhjMTc4YjAwOGU2MTU3YjhiOTYzYTZmOWEzNzI2IiwiZmlyc3RuYW1lIjoiSXNhYWMiLCJsYXN0bmFtZSI6IlRhbiIsInR5cGUiOiJhZG1pbiIsInN0YXR1cyI6IjEifX0.jzbXtfJCR_c-rED5B8nMq49ZISRka6Gbdv7qlSmIOm7F8pQBazPKUNdXzZxnr875zsRDqfATewAR7rWhnjPuJ8ilBZbgeE0SojUyrVxP-VZQdT-gqmUTKmau-jUtSEJWTxqRdv98PnaFozZaSJaFLrATlhDic90rrEVCJjhQzpWA4ia6GecIHtHdDXSOTV6TswygsaYSpfAmueme4g6--r5_jyxBwhggrFF2LVz56Px6OGFt_jAOPmAjzIKwdTUI30VZiIgeIFp-EEEry7DoS8M9t10koyxb6AsL2dCCqdlLBgQ9MQ311OTchHDabEvOvMzvw10909ZDHEJkKpN39w

- The header value is the response user_token when the user logged in.

*/
$app->group('', function() use ($app){

    $app->get('/getUser/{user_id}', 'Module\Controllers\TestController:getUser');

})->add(new \Module\Middlewares\Auth());




