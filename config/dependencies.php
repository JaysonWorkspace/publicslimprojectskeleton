<?php

class DI{
    
    public $Container;
    public $Settings;
    public $C;

    public function __construct($c){

        $this->C         = $c;
        $this->Settings  = $c->get('settings');
        $this->Container = $this->getContainer();
    }

    public function __invoke() { 

        return $this->Container;

    } 

    public function getContainer(){

        return [
            'template'          => $this->template(),
            'email'             => $this->email(),
            'flash'             => $this->flash(),
            'validator'         => $this->gump(),
            'logger'            => $this->logger(),
            'mpdf'              => $this->mpdf(),
            'database'          => new Medoo\Medoo($this->Settings['database']),
            'database2'         => new Medoo\Medoo($this->Settings['database2'])
        ];

    }

    public function template(){

        $view = new \Slim\Views\Twig($this->Settings['view']['template_path'], $this->Settings['view']['twig']);
        $view->addExtension(new Slim\Views\TwigExtension($this->C->get('router'), $this->C->get('request')->getUri()));
        $view->addExtension(new Twig_Extension_Debug());

        return $view;
    }

    public function email(){

        if(strtolower($_SERVER['SERVER_NAME']) === 'localhost'){

            $slash = "\\";
            $email_templates = '\email_templates';

        }else{

            $slash = "/";
            $email_templates = '/email_templates';
        }

        $dirname = dirname(__FILE__);
        $dirname = explode($slash, $dirname);

        $arr_dirname = [];
        foreach ($dirname as $key => $value) {

            if($key == count($dirname) - 1){
                break;
            }

            $arr_dirname[] = $value;
        }

        $dirname = implode($slash, $arr_dirname);
        $dirname .= $email_templates;

        $loader = new Twig_Loader_Filesystem($dirname);
        $twig   = new Twig_Environment($loader);   
        
        return $twig;

    }

    public function flash(){

        return new Slim\Flash\Messages;
    }

    public function gump(){

        return new GUMP;
    }

    public function logger(){

        $logger = new Monolog\Logger($this->Settings['logger']['name']);
        $logger->pushProcessor(new Monolog\Processor\UidProcessor());
        $logger->pushHandler(new Monolog\Handler\StreamHandler($this->Settings['logger']['path'], \Monolog\Logger::DEBUG));

        $this->C['logger'] = $logger;

        return $logger;
    }

    public function mpdf(){

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => [190, 236],
            'orientation' => 'L'
        ]);

        return $mpdf;
    }

}

