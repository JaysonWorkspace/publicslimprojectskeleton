<?php

// To help the built-in PHP dev server, check if the request was actually for
// something which should probably be served as a static file
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}

require_once "vendor/autoload.php";

session_start();

$app = new Slim\App((new AppSettings)->Settings);

$container = $app->getContainer();

$container["errorHandler"] = function ($c) {
    return new \Module\Handlers\ExceptionErrorHandler($c["logger"]);
};

$container["phpErrorHandler"] = function ($c) {
    return new \Module\Handlers\PhpErrorHandler($c["logger"]);
};

$container["notAllowedHandler"] = function ($c) {
    return new \Module\Handlers\MethodNotAllowedErrorHandler($c["logger"]);
};

$container["notFoundHandler"] = function ($c) {
    return new \Module\Handlers\NotFoundErrorHandler($c["logger"]);
};

Container::set((new DI(($container)))->Container);

require __DIR__ . '/config/routes.php';

$app->run();
