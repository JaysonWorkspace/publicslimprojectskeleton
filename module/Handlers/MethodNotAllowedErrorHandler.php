<?php  
 
namespace Module\Handlers; 

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface; 
 
 
class MethodNotAllowedErrorHandler {   

    protected $logger; 
   
    public function __construct(LoggerInterface $logger) { 

        $this->logger = $logger; 
    } 
 
 
    public function __invoke(Request $request, Response $response, array $methods){ 
 
        $str_message = 'Method must be one of: '. $methods[0]; 

        $message = [];
        $message['message'] = $str_message;
        $message['status']  = 405;
        $message['code']    = $message['status'];

        $this->logger->critical($str_message); 
      
        return $response->withJSON($message, $message['status']); 
    } 

 
} 