<?php 

namespace Module\Handlers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Throwable;

class PhpErrorHandler {	

	protected $logger;
	
	public function __construct(LoggerInterface $logger){

		$this->logger = $logger;
	}


	public function __invoke(Request $request, Response $response, Throwable $error){

		$message = [];
		$message['message']	= $error->getMessage();
		$message['line']	= $error->getLine();
		$message['file']	= $error->getFile();
		$message['status']	= $error->getCode() == 0 ? 500 : $error->getCode();
		$message['code']    = $message['status'];

		$this->logger->critical($error->getMessage());
     
        return $response->withJSON($message, $message['status']);
	}

}