<?php 

namespace Module\Services;

class REST{

	public function GET_AUTH($url, $authorization){

		$ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, 
        		array(
                        "cache-control: no-cache",
					    "Authorization:" .  $authorization
                    )
        );

        $data = curl_exec($ch);
        
        $err = curl_error($ch);

		curl_close($ch);

		if ($err) {
			return 'curl error';
		} 

		return $data;
	}

    public function GET_NO_AUTH($url){

        return $url;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Accept-Encoding: gzip"
        ));

        $data = curl_exec($ch);

        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
            return false;
        } 

        if($data){
            $data = gzdecode($data);
        }

        return $data;
    }

    public function POST_NO_AUTH($url, $body){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Accept-Encoding: gzip"
        ));

        $data = curl_exec($ch);

        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
            return false;
        } 

        if($data){
            $data = gzdecode($data);
        }

        return $data;
    }

    public function POST_DATA($url, $body){

        $curl = curl_init();

          curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 540,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $body,
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json"
          ),
        ));

        $data = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        }

        return $data;
    }

    public function PUT_DATA($url, $body){

        $curl = curl_init();

          curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 540,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => $body,
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json"
          ),
        ));

        $data = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        }

        return $data;
    }
	

}