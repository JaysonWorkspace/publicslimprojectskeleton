<?php

namespace Module\Models;

use Module\Helpers\MedooHelper;

class TestModel extends MedooHelper {

    public function __construct($c){

        parent::__construct($c);
    }

    public function getUsers(){

        $users_details = parent::select($this->database, 'user', 0 , '', 0);
        
        return $users_details;
    }

    public function getUser($parameters){

        $user_details = parent::get($this->database, 'user', 0 , '', $parameters);
        
        return $user_details;
    }

    public function checkUsername($username){

        $details = parent::get($this->database, 'user', 0 , '', $username);
        
        return $details;
    }

    public function addUser($parameters){

        $encryt_password = $this->Tool->encrypt_decrypt('encrypt', $parameters['password']);

        $parameters['password'] = $encryt_password;

        $add_user = parent::insert($this->database, 'user', $parameters);
        
        return $add_user;
    }

    public function updateUser($parameters){

        $where = [
            'user_id' => $parameters['user_id']
        ];

        $params_values  = array_keys($parameters);

        $previous_data   = parent::get($this->database, 'user', 0 , $params_values, $where);

        $previous_data['password'] = $this->Tool->encrypt_decrypt('decrypt', $previous_data['password']);

        $parameters['password']   = $this->Tool->encrypt_decrypt('encrypt', $parameters['password']);

        $updateUser = parent::update($this->database, 'user', $parameters, $where);

        if(!$updateUser){
            
            if($previous_data == $parameters){
                return 0;
            }{
                return false;
            }
        }
        
        return $updateUser;
    }

    public function deleteUser($parameters){

        $where['user_id'] = $parameters['user_id'];

        $delete_user = parent::delete($this->database, 'user', $where);
        
        return $delete_user;
    }

}
