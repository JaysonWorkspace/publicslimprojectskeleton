<?php

namespace Module\Models;

use Module\Helpers\MedooHelper;

class LoginModel extends MedooHelper {

	public function __construct($c){

		parent::__construct($c);
	}

	public function user_details($parameters){

		$where = [
			'username' => $parameters['username']
		];

		$user_details = parent::get($this->database, 'user', 0 , '', $where);

		return $user_details;
	}

	public function create_user_token($userDetails){

		$requestToken = $this->Tool->generateToken();

		$authToken = $this->Tool->generateJWS($userDetails, $requestToken);

		$results = [
			'user_token'   => $authToken,
			'user_details' => $userDetails
		];

		return $results;
	}


}


?>