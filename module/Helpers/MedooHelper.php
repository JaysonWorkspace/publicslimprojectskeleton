<?php

namespace Module\Helpers;

class MedooHelper extends SettingsHelper{

	public function __construct($c){

		parent::__construct($c);

		$this->ResponseCodes = new \ResponseCodes();
		$this->Tool 		 = new \Tool();
		$this->REST 		 = new \Module\Services\REST;
	}

	public function insert($database, $table_name, $data){

		//to debug uncomment the code below
		//$database->debug();
		//$result = $database->insert($table_name, $data);
		//var_dump($result);exit;

		$database->insert($table_name, $data);

		return $database->id();
	}

	public function formatWhere($where){

		if(!is_array($where)){
			$where = [];
		}

		if(count($where) > 1){
			
			$temp = [];
	        $temp['AND'] = $where;
	       	$where = $temp;
		}

		return $where;

	}

	public function delete($database, $table, $where){	
		
		$where = $this->formatWhere($where);

		if($where == 0){
			$where = [];
		}

		//to debug uncomment the code below
		//$database->debug();

		$result = $database->delete($table, $where);

		//var_dump($result);exit;
		
		return $result->rowCount();

	}

	public function update($database, $table, $data, $where){	
		
		$where = $this->formatWhere($where);

		if($where == 0){
			$where = [];
		}

		//to debug uncomment the code below
		//$database->debug();

		$result = $database->update($table, $data, $where);

		//var_dump($result);exit;

		return $result->rowCount();

	}

	public function select($database, $table_name, $join, $column, $where){

		$where = $this->formatWhere($where);

		if($column == ""){

			$column = "*";

		}

		if($where == 0){
			$where = [];
		}	

		if($join != 0){

			//to debug uncomment the code below
			//$database->debug();

			$result = $database->select($table_name, $join, $column, $where);

			//var_dump($result);exit;

		} else {

			//to debug uncomment the code below
			//$database->debug();

			$result = $database->select($table_name, $column, $where);
			
			//var_dump($result);exit;
		}

		return $result;

	}


	public function get($database, $table_name, $join, $data, $where){

		$where = $this->formatWhere($where);

		if($data == ""){
			$data = "*";
		}

		if($where == 0){
			$where = [];
		}
			
		if($join != 0){

			//to debug uncomment the code below
			//$database->debug();

			$result = $database->get($table_name, $join, $data, $where);

			//var_dump($result);exit;
			
		} else {

			//to debug uncomment the code below
			//$database->debug();

			$result = $database->get($table_name, $data, $where);

			//var_dump($result);exit;
		}

		return $result;

	}

	public function startTransaction($database){

		return $database->pdo->beginTransaction();

	}

	public function rollBack($database){

		return $database->pdo->rollBack();

	}

	public function commit($database){

		return $database->pdo->commit();

	}


}