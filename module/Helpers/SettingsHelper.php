<?php

namespace Module\Helpers;

class SettingsHelper{

        public function __construct($c){

        $this->email 			= \Container::get('email');
        $this->flash  			= \Container::get('flash');
        $this->validator		= \Container::get('validator');
        $this->logger  			= \Container::get('logger');
        $this->database  		= \Container::get('database');
        $this->database2  		= \Container::get('database2');
        $this->mpdf  			= \Container::get('mpdf');
	}

}