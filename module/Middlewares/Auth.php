<?php
namespace Module\Middlewares;

use Namshi\JOSE\SimpleJWS;
use ResponseCodes;

class Auth{

	public function __invoke($request, $response, $next){

		$token = false;	

		if ($request->hasHeader('AUTHORIZATION')) {
			$token = $request->getHeader('AUTHORIZATION')[0];
		}

		if($token) {

			try {

         		$jws = SimpleJWS::load($token);

         		if ($jws->verify(file_get_contents(__DIR__."/../../key/public.key"), 'RS256')) {
						
					$payload = $jws->getPayload();
					
					$request = $request->withAttribute('token',  $payload);
					$request = $request->withAttribute('userid', $payload['data']['user_id']);
					$request = $request->withAttribute('user',   $payload['data']);

					$response = $next($request, $response);

					return $response;
				}

     		} catch (\InvalidArgumentException $e) {

        		$message = ResponseCodes::get(0, 0002);
				return $response->withJSON($message, 401);
     		}

		}

		$message = ResponseCodes::get(0, 0003);
		
		return $response->withJSON($message, 401);
	}

}
