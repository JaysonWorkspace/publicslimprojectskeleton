<?php 

namespace Module\Controllers;

class TestController{

	private $TestModel;

	public function __construct($c){

		$this->TestModel = new \Module\Models\TestModel($c);
	}

    public function hello($req, $res, $arg){

        //$test = $this->TestModel->REST->GET_NO_AUTH('http://test.com/');

        return $res->withJSON('Hello There!', 200);
    }

    public function getUsers($req, $res, $arg){

    	$result = $this->TestModel->getUsers();

    	if(!$result){

    		$message = $this->TestModel->ResponseCodes->get(0, '0010');

			return $res->withJSON($message, 400);
    	}

        return $res->withJSON($result, 200);
    }

    public function getUser($req, $res, $arg){

        //$user = $req->getAttribute('user');
        //var_dump($user);exit;//to get user details

    	$user_id = $req->getAttribute('user_id');

    	$validationRules = [
            'user_id'    => 'required|numeric'
        ];

        $parameters['user_id'] = $user_id;

        $this->TestModel->validator->validation_rules($validationRules);

        $isValid = $this->TestModel->validator->run($parameters);

        if($isValid === false){

        	$message = $this->TestModel->validator->get_errors_array();

        	return $res->withJSON($message, 400);
        }

    	$result = $this->TestModel->getUser($parameters);

    	if(!$result){

    		$message = $this->TestModel->ResponseCodes->get(0, '0010');

			return $res->withJSON($message, 400);
    	}

        return $res->withJSON($result, 200);
    }

    public function addUser($req, $res, $arg){

        $parameters = $req->getParsedBody();

        $validationRules = [
                        'username'  => 'required',
                        'password'  => 'required',
                        'firstname' => 'required',
                        'lastname'  => 'required',
                        'type'      => 'required'
                    ];

        $isValid = $this->TestModel->validator->validate($parameters, $validationRules);

        if($isValid !== true){

            $message = $this->TestModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $username['username'] = $parameters['username'];

        $check_username = $this->TestModel->checkUsername($username);

        if($check_username){

            $message = $this->TestModel->ResponseCodes->get(0, '0011');

            return $res->withJSON($message, 400);
        }

        $result = $this->TestModel->addUser($parameters);

        if(!$result){

            $message = $this->TestModel->ResponseCodes->get(0, '0006');
            
            return $res->withJSON($message, 400);
        }

        $message = $this->TestModel->ResponseCodes->get(1, '1000');

        return $res->withJSON($message, 200);
    }

    public function updateUser($req, $res, $arg){

        $parameters = $req->getParsedBody();

        $validationRules = [
                        'user_id'   => 'required|numeric'
                    ];

        $isValid = $this->TestModel->validator->validate($parameters, $validationRules);

        if($isValid !== true){

            $message = $this->TestModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $result = $this->TestModel->updateUser($parameters);

        if($result === 0){

            $message = $this->TestModel->ResponseCodes->get(0, '0008');
            
            return $res->withJSON($message, 400);
        }
        
        if($result === false){

            $message = $this->TestModel->ResponseCodes->get(0, '0007');
            
            return $res->withJSON($message, 400);
        }

        $message = $this->TestModel->ResponseCodes->get(1, '1001');

        return $res->withJSON($message, 200);
    }

    public function deleteUser($req, $res, $arg){

        $parameters = $req->getParsedBody();

        $validationRules = [
                        "user_id"  => "required|numeric"
                    ];

        $isValid = $this->TestModel->validator->validate($parameters, $validationRules);

        if($isValid !== true){

            $message = $this->TestModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $result = $this->TestModel->deleteUser($parameters);

        if(!$result){

            $message = $this->TestModel->ResponseCodes->get(0, '0009');
            
            return $res->withJSON($message, 400);
        }

        $message = $this->TestModel->ResponseCodes->get(1, '1002');

        return $res->withJSON($message, 200);
    }

}