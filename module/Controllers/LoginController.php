<?php 

namespace Module\Controllers;

class LoginController{

	private $LoginModel;

	public function __construct($c){

		$this->LoginModel = new \Module\Models\LoginModel($c);
	}

	public function login($req, $res, $arg){

		$parameters = $req->getParsedBody();

		$validationRules = [
            'username'    => 'required|alpha_numeric|max_len,100|min_len,6',
    		'password'    => 'required|alpha_numeric|max_len,100|min_len,4'
        ];

        $filterRules = [
        	'username' => 'trim|sanitize_string',
    		'password' => 'trim|sanitize_string'
        ];

        $this->LoginModel->validator->validation_rules($validationRules);

        $this->LoginModel->validator->filter_rules($filterRules);

        $isValid = $this->LoginModel->validator->run($parameters);

        if($isValid === false){

        	$message = $this->LoginModel->validator->get_errors_array();

        	return $res->withJSON($message, 400);
        }

		$userDetails = $this->LoginModel->user_details($parameters);

		if(!$userDetails){

			$message = $this->LoginModel->ResponseCodes->get(0, '0004');

			return $res->withJSON($message, 400);
		}

		if($userDetails['status'] == 0){

			$message = $this->LoginModel->ResponseCodes->get(0, '0005');

			return $res->withJSON($message, 400);
		}

		$isMatch = $this->LoginModel->Tool->match_encrypted($parameters['password'], $userDetails['password']);

		if(!$isMatch){

			$message = $this->LoginModel->ResponseCodes->get(0, '0004');
			
			return $res->withJSON($message, 400);
		}

		$result = $this->LoginModel->create_user_token($userDetails);

        return $res->withJSON($result, 200);

	}



}